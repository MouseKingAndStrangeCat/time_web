"""Кейсы бизнес логики для file namespace."""
import os

from flask import (
    send_from_directory,
    Response,
)

from base import producer
from file_provider.dto import Workdir


def get_file(file_name: str) -> Response:
    """
    Логика скачивания файла.

    :param file_name: Имя файла.
    :return: Файл.

    """
    workdir = Workdir(name=os.environ.get('WORKDIR'))

    path = None
    for root, _, files in os.walk(workdir.name):
        if file_name in files:
            path = root
            break

    return send_from_directory(
        directory=path or workdir.name,
        filename=file_name,
        as_attachment=True,
    )


def create_file(path: str):
    """
    Логика создания файла на сервере.

    :param path: Путь, для создания файла.

    """
    producer.send_message(
        queue=os.environ.get('FILE_UPLOAD_QUEUE', 'unknown'),
        message=path,
    )
