"""Кейсы бизнес логики для file_structure namespace."""
import os
from typing import (
    Optional,
    List,
)

from file_provider.dto import Workdir


class GetFileStructureUseCase:
    """Логика считывания файловой структуры."""

    START_DEEP = 1
    DEEP_STEP = 1
    DEFAULT_DEEP = 2

    def __init__(self, deep_search: Optional[int]):
        """
        Инициализация юз кейса.

        :param deep_search: Глубина считывания файловой структуры.

        """
        self.deep_search = deep_search or self.DEFAULT_DEEP
        self.workdir = Workdir(name=os.getenv('WORKDIR'))
        self.file_structure: List[Optional[str]] = []

    def depth_first_search(self, dirs: List[str], directory: str, current_deep: int):
        """
        Рекурсивный поиск в глубину файлов с ограничением на глубину.

        :param dirs: Список объектов в текущей директории.
        :param directory: Текущая директория.
        :param current_deep: Текущий уровень вложенности поиска.

        """
        for file in dirs:
            new_path = f'{directory}{file}'

            if os.path.isdir(new_path):
                if current_deep >= self.deep_search:
                    continue

                self.depth_first_search(
                    dirs=os.listdir(path=new_path),
                    directory=f'{new_path}/',
                    current_deep=current_deep + self.DEEP_STEP,
                )
            else:
                # [len(self.workdir.name):] - удаление информации о рабочей директории
                self.file_structure.append(
                    new_path[len(self.workdir.name):]
                )

    def execute(self) -> List[Optional[str]]:
        """
        Выполняет юз кейс.

        :return: Файловая структура.

        """
        self.depth_first_search(
            dirs=os.listdir(path=self.workdir.name),
            directory=self.workdir.name,
            current_deep=self.START_DEEP,
        )

        return self.file_structure
