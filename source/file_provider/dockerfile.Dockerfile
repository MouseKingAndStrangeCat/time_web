FROM python:3.7.3
MAINTAINER gitlab: @iwannabeurgeneric

WORKDIR /opt/project/

RUN pip install --upgrade pip
RUN pip install gunicorn[gevent]
ADD requirements.txt /opt/project/
RUN pip install -r requirements.txt

ADD source/base/ /opt/project/source/base/
ADD source/file_provider/ /opt/project/source/file_provider/

WORKDIR /opt/project/source/
