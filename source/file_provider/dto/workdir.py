"""Модель данных для отправки строки файла по сети."""
from pydantic import (
    BaseModel,
    validator,
    ValidationError,
)

from file_provider.constants import ROOT_DIR
from file_provider.exceptions import WorkdirDoesNotExist


class Workdir(BaseModel):
    """Представление рабочей директории."""

    name: str

    @validator('name')
    def name_validation(cls, value):  # noqa: U100
        """
        Проверяет название пути переданное в модель.

        :param value: Входящее значение.

        """
        if not value:
            raise WorkdirDoesNotExist('Workdir does not exist')
        if value[0] != '/':
            raise ValidationError('Path is not absolute')
        if value[-1:] != '/':
            raise ValidationError('You must write a slash at the end of the path')
        return f'{ROOT_DIR}{value}'
