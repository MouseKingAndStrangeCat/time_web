"""Содержит неймспейс для работы с сущностью файла."""
import flask
from flask import (
    request,
    Response,
)
from flask_restplus import (
    Namespace,
    Resource,
)
from flask_restplus.reqparse import ParseResult
from pydantic import ValidationError

from base import api
from file_provider.constants import FILE
from file_provider.exceptions import WorkdirBaseException
from file_provider.serializers import (
    file_get_parser,
    file_post_parser,
)
from file_provider.use_cases import (
    create_file,
    get_file,
)


file_ns = Namespace(
    name=FILE,
    description='file endpoints',
)


@file_ns.route('/')
class FileStructureAPI(Resource):
    """Эндпоинты для работы с сущностью файла."""

    @api.expect(file_get_parser)
    def get(self) -> Response:
        """Скачивание файла."""
        data: ParseResult = file_get_parser.parse_args(request)
        try:
            file: Response = get_file(
                file_name=data.path,
            )
        except ValidationError:
            return flask.jsonify({
                'message': 'Wrong work directory name',
                'success': False,
            })
        except WorkdirBaseException as error:
            return flask.jsonify({
                'message': str(error),
                'success': False,
            })
        else:
            return file

    @api.expect(file_post_parser)
    def post(self):
        """Создание файла на сервере."""
        data: ParseResult = file_post_parser.parse_args(request)
        create_file(path=data.path)
        return flask.jsonify({'success': True})
