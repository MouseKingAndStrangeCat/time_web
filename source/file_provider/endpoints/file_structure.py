"""Содержит неймспейс для работы с сущностью файловой структуры."""
from typing import (
    Any,
    Dict,
)

import flask
from flask import request
from flask_restplus import (
    Namespace,
    Resource,
)
from flask_restplus.reqparse import ParseResult
from pydantic import ValidationError

from base import (
    api,
)
from file_provider.constants import FILE_STRUCTURE
from file_provider.exceptions import WorkdirBaseException
from file_provider.serializers import file_structure_get_parser
from file_provider.use_cases import GetFileStructureUseCase


file_structure_ns = Namespace(
    name=FILE_STRUCTURE,
    description='file structure endpoints',
)


@file_structure_ns.route('/')
class FileStructureAPI(Resource):
    """Эндпоинты для работы с сущностью файловой структуры."""

    @api.expect(file_structure_get_parser)
    def get(self) -> Dict[str, Any]:
        """Получение информации о файловой структуре."""
        data: ParseResult = file_structure_get_parser.parse_args(request)
        try:
            file_structure = GetFileStructureUseCase(
                deep_search=data.deep_search,
            ).execute()
        except WorkdirBaseException as error:
            return flask.jsonify({
                'message': str(error),
                'success': False,
            })
        except ValidationError:
            return flask.jsonify({
                'message': 'Wrong work directory name',
                'success': False,
            })
        except FileNotFoundError:
            return flask.jsonify({
                'message': 'Wrong work directory or permission denied',
                'success': False,
            })
        else:
            return flask.jsonify({
                'file_structure': file_structure,
                'success': True,
            })
