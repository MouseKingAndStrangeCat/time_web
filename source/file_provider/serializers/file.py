"""Сериализаторы данных для file namespace."""
from flask_restful.reqparse import RequestParser

from base import api


file_get_parser: RequestParser = api.parser()
file_get_parser.add_argument(
    name='path',
    required=True,
    type=str,
    help='example: 1GB.bin',
)

file_post_parser: RequestParser = api.parser()
file_post_parser.add_argument(
    name='path',
    required=True,
    type=str,
    help='example: 1GB.bin',
)
