"""Сериализаторы данных для file_structure namespace."""
from flask_restful.reqparse import RequestParser

from base import api


file_structure_get_parser: RequestParser = api.parser()
file_structure_get_parser.add_argument(
    name='deep_search',
    required=False,
    type=int,
    help='example: 2',
)
