# Документация к сервису file_provider
Сервис для скачивания тяжёлых файлов.

## Билд сервиса
````
docker-compose -f source/file_provider/docker_compose.yml up --build -d file_provider
````

## Swagger
Доступен если в env переменная DEBUG установлена в True, ссылка на доку:
````
http://0.0.0.0:5000/swagger
````

## Как дополнительно настроить nginx для данного сервиса
To prevent nginx from waiting we need to add a new line to the configuration.

Edit /etc/nginx/sites-available/flaskconfig

````
server {
    listen 80;
    server_name localhost;

    location / {
        include uwsgi_params;
        uwsgi_pass unix:///tmp/flask.sock;
        uwsgi_buffering off;  # <-- this line is new
    }
}
````

The line uwsgi_buffering off; tells nginx not to wait until a response it complete.

Restart nginx: sudo service nginx restart and look at localhost/time/ again.
