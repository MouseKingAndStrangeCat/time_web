"""Сборка и запуск сервиса."""
from base import (
    api,
    application,
)
from file_provider.constants import (
    APP_NAME,
    APP_VERSION,
    FILE_STRUCTURE,
    FILE,
)
from file_provider.endpoints import (
    file_structure_ns,
    file_ns,
)


api.title = APP_NAME
api.version = APP_VERSION

api.add_namespace(
    ns=file_structure_ns,
    path=f'/{APP_VERSION}/{FILE_STRUCTURE}',
)

api.add_namespace(
    ns=file_ns,
    path=f'/{APP_VERSION}/{FILE}',
)


if __name__ == '__main__':
    application.run(port=5000)
