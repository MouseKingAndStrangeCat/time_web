"""Константы для пакета file_provider."""
import os


APP_NAME: str = os.getenv('APP_NAME', 'unknown')
APP_VERSION: str = os.getenv('APP_VERSION', 'unknown')
FILE_STRUCTURE = 'file_structure'
FILE = 'file'
ROOT_DIR = 'root_dir'
