"""Исключения для пакета file_provider."""


class FileProviderServiceBaseException(Exception):
    """Базовое исключение для сервиса загрузок."""


class WorkdirBaseException(FileProviderServiceBaseException):
    """Базовое исключение для сущности workdir."""


class WorkdirDoesNotExist(WorkdirBaseException):
    """Бросается, когда рабочая директория не указана."""
