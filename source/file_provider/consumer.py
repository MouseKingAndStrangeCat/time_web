"""Консьюмер, сохраняющий файлы на сервер."""
import os
import shutil

import pika

from file_provider.dto import Workdir


class Consumer:

    DEFAULT_HOST = '127.0.0.1'

    def __init__(self):
        self.queue_name = os.environ.get('FILE_UPLOAD_QUEUE', 'unknown')
        self.connection = pika.BlockingConnection(pika.ConnectionParameters(
            host=os.environ.get('RABBITMQ_HOST', self.DEFAULT_HOST),
        ))
        self.channel = self.connection.channel()
        self.channel.queue_declare(queue=self.queue_name)
        self.workdir = Workdir(name=os.environ.get('WORKDIR'))

    def callback(self, ch, method, properties, body: bytes):
        """Создание нового фалйа в указанной директории."""
        try:
            new_file_path = Workdir(name=body.decode('utf-8'))
            new_file_path.name = body.decode('utf-8')

            destination = f'{self.workdir.name}{new_file_path.name[1:]}'
            if not os.path.exists(destination):
                # Тут не факт, что создаться большая вложенность, оставляю так нарочно
                os.mkdir(destination)

            shutil.copy2(
                src=os.environ.get('BASE_FILE', ''),
                dst=destination,
            )
        except FileNotFoundError:
            # Тут различные действия когда нет файла для копирования
            pass
        except OSError:
            # Тут различные действия когда не хватает прав на создание папки например.
            pass

    def run(self):
        """Чтение консьюмером новых сообщений."""
        self.channel.basic_consume(
            queue=self.queue_name,
            on_message_callback=self.callback,
            auto_ack=True,
        )
        self.channel.start_consuming()


if __name__ == '__main__':
    Consumer().run()
