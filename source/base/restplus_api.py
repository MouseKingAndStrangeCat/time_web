"""Настройка рестплюс фреймворка."""
from flask_restplus import (
    Api,
    Resource,
)

from base.constants import DEBUG
from base.restplus_app import (
    application,
    exc_handler,
)


api = Api(
    application,
    doc='/swagger/' if DEBUG else None,  # Для отключения swagger на проде
)


@api.errorhandler
def default_error_handler(error):
    """Дефолтный обработчик ошибок."""
    return exc_handler(error)


@api.route('/health-check')
class HealthCheckApi(Resource):
    """Проверка работоспособности апи."""

    def get(self):
        """Суть показывать, что апи работоспособно."""
        return 'OK', 200
