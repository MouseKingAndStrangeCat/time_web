"""Содержит коннект к rabbitmq для отправки сообщений."""
import os
from typing import List

import pika

from base.singleton import Singleton


class Producer(Singleton):

    DEFAULT_HOST = '127.0.0.1'
    QUEUES: List[str] = [
        os.environ.get('FILE_UPLOAD_QUEUE', 'unknown'),
    ]

    def __init__(self):
        self.connection = None
        self.connection = pika.BlockingConnection(pika.ConnectionParameters(
            host=os.environ.get('RABBITMQ_HOST', self.DEFAULT_HOST),
        ))
        self.channel = self.connection.channel()

        # Существующие очереди
        for queue in self.QUEUES:
            self.channel.queue_declare(queue=queue)

    def __del__(self):
        if self.connection:
            self.connection.close()

    def send_message(self, queue: str, message: str):
        """
        Отправка сообщения в очередь.

        :param queue: Имя очереди.
        :param message: Сообщений для консьюмера.

        """
        self.channel.basic_publish(
            exchange='',
            routing_key=queue,
            body=message,
        )


producer = Producer()
