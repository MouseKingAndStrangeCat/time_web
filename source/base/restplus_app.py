"""Настройка фласк фреймворка."""
import flask
from werkzeug.exceptions import HTTPException

from base.constants import DEBUG


application = flask.Flask(__name__)

application.config['SWAGGER_UI_DOC_EXPANSION'] = 'list'
application.config['RESTPLUS_VALIDATE'] = True
application.config['RESTPLUS_MASK_SWAGGER'] = True
application.config['ERROR_404_HELP'] = True
application.debug = DEBUG
application.url_map.strict_slashes = False


def exc_handler(exc):
    if isinstance(exc, HTTPException):
        return flask.jsonify({'success': False, 'message': exc.description}), exc.code
    return flask.jsonify({'success': False, 'message': 'An unhandled exception occurred.'}), 500


@application.errorhandler(Exception)
def handle_500(error):
    """Обработка ошибки 500."""
    return exc_handler(error)
