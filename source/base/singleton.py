"""Метакласс для создание singleton объекта."""
from typing import (
    Any,
    Dict,
    Type,
)


class _Singleton(type):
    """Метакласс для создание singleton объекта."""

    _instances: Dict[Type, Any] = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(_Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]


class Singleton(_Singleton('SingletonMeta', (object,), {})):  # type: ignore
    """Базовый класс для создания синглтона."""
