# Задание:

Допустим, на сервере есть директория, которая хранит различные файлы. 
Размер файлов может быть весьма большим, например 1гб и более.
Требуется написать REST API сервис на python3 + flask со следующим функционалом:

1. Получение структуры директории
2. Скачивание zip архива с содержимым выбранной директории

Ограничений на формат данных и структуру API нет, эндпоинтов при необходимости может быть больше двух.
Пользователь сервиса не должен знать о местоположении корневой директории, с которой работает сервис.
На сервере достаточно свободного места для создания архива с любой директорией.
Для развёртывания использовать Docker.
Сервис не должен пятисотить.
При развёртывании сервиса должна быть возможность указать с какой директорией на сервере он должен работать.


В readme описать процесс деплоя.
Тесты необязательны.

В целом, почти полная свобода реализации :)

# Энивей, предложение по доработке
1. Сделать функционал по скачиванию зип архива с содержимым директории. Создание архива, 
   во избежание повисания сервиса надолго, необходимо делать в отдельном процессе. Использовать 
   для этого воркеры и очередь задач, или юзать мультипроцессинг и хранить айдишки процессов 
   где-нибудь самому не суть важно. Получение архива соответственно необходимо сделать 
   по отдельному урлу.
2. Сделать возможность задавать целевую директорию на хосте с помощью энвайронментов без 
   необходимости лезть в докер композ.
3. Выкинуть код, не относящийся к реализации и заданию, а также весь неиспользуемый код. 
   И ради приличия переименовать название приложения.
4. Поправить выдачу file_structure, которая все элементы выплёвывает со слешем на конце. 
   Как мне, как пользователю, определить что из выдачи директория, а что файл?
5. Подумать, почему нельзя складывать пути конкатенацией строк и заодно попробовать задать 
   workdir без слэша, или не абсолютным.
6. Пересмотреть структуру проекта, ибо она слишком запутана для ста строк кода. Излишняя 
   декомпозиция вредит читабельности.

# Комментарии разработчика по доработке
1. Функционал со скачиванием архива доступен и улучшен, на счёт создание архива не вопрос, 
   я реализовал, но изначальное тз на счёт создания архива вызывает ряд вопросов. При создании 
   файла сделано допущение, что в .env указан путь к файлу, из которого создаётся новый файл.
2. В .env стоит указать WORKDIR, сервис будет работать именно с этой директорией как и в 
   предыдущей версии. Очень важный момент, что сервису должны права быть розданы на эту 
   директорию, в противном случае доступа не будет, chown все дела.
3. Выкинул авторизацию из настройки апи потому, что решил не использовать её на тестовом. Всё 
   остальное, например хелс чек которого не было в тз считаю корректным оставить, аргументация 
   есть, готов к обсуждению. 
   Переименовал download_service в file_provider. Основной функционал был по скачиванию файлов 
   и я посчитал адекватным назвать сервис для скачивания файлов, как сервис загрузок. Возможно 
   имелся ввиду APP_NAME=road_service в .env файле, это поправил, от пет проджекта другого 
   осталось легаси.
4. Поправил + в выдаче присутствуют только файлы ибо они доступны для скачивания, то есть 
   нарочно так сделано.
5. Да я понимаю почему, если например кто-то случайно напишет в .env путь например без слеша на 
   конце работать уже будет не так, как хотелось бы. Такую ошибку допустить достаточно легко, 
   особенно если сервис писал не ты, понимаю. В качестве решения было два пути, первый путь я 
   мог поменять логику работы с директориями, но я выбрал второй, а именно показать что я знаю и 
   умею работать с важными для билинга dto объектами. Таким образом я гарантировал себе, что слеш 
   на конце будет и в начале тоже будет. Вообще dto я думаю 100% писать в билинге и мне захотелось 
   показать это.
6. В этом пункте я к сожалению не согласен, моя позиция другая, готов обосновать свою позицию в 
   живую, если кратко то я не могу знать как в дальнейшем будет развиваться сервис, какие фичи 
   будут туда заливаться, поэтому лучше со старта соблюдать принципы чистой архитектуры даже 
   если кода не много, что бы в будущем не занимать у бизнеса время на рефактор.
