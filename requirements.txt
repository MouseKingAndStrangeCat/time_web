Flask==1.0.2
Flask-HTTPAuth==3.3.0
Flask-RESTful==0.3.6
flask-restplus==0.13.0
pika==1.1.0
pydantic~=1.7.3
Werkzeug==0.16.1
ujson~=1.35
